<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view("page.register");
    }

    public function kirim(Request $request)
    {
       //dd($request->all()); 
       $nama = $request["nama_lengkap"];
       $alamat = $request["address"];

       return view("page.welcome", compact("nama", "alamat"));
    }
}
